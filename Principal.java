import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Principal extends JFrame implements ActionListener{

	private JMenuBar mb;
	private JMenu menuArchivo, menuOpciones, menuAcercaDe, menuColorFondo;
	private JMenuItem miCalculo, miRojo, miNegro, miMorado, miElCreador, miSalir, miNuevo;
	private JLabel labelLogo, labelBienvenido, labelTitle, labelNombre, labelAPaterno, labelDepartamento, labelAntiguedad, labelResultado, labelfooter;
	private JTextField txtNombreTrabajador, txtAPaterno;
	private JComboBox comboDepartamento, comboAntiguedad;
	private JScrollPane scrollpane1;
	private JTextArea textarea1;
	private JButton boton1;

	String nombreAdministrador = "";

	public Principal(){
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Pantalla Principal");
		getContentPane().setBackground(new Color(255,0,0)); //color de fondo
		setIconImage(new ImageIcon(getClass().getResource("images/icon.png")).getImage());

		Bienvenida ventanaBienvenida = new Bienvenida();
		nombreAdministrador = ventanaBienvenida.texto;

		mb = new JMenuBar();
		mb.setBackground(new Color(255,0,0));
		setJMenuBar(mb);

		menuArchivo = new JMenu("Archivo");
		menuArchivo.setBackground(new Color(255,0,0));
		menuArchivo.setForeground(new Color(255,255,255));
		menuArchivo.setFont(new Font("Andale Mono", 1, 14));
		mb.add(menuArchivo);

		menuOpciones = new JMenu("Opciones");
		menuOpciones.setBackground(new Color(255,0,0));
		menuOpciones.setForeground(new Color(255,255,255));
		menuOpciones.setFont(new Font("Andale Mono", 1, 14));
		mb.add(menuOpciones);

		menuAcercaDe = new JMenu("Acerca de");
		menuAcercaDe.setForeground(new Color(255,255,255));
		menuAcercaDe.setBackground(new Color(255,0,0));
		menuAcercaDe.setFont(new Font("Andale Mono", 1, 14));
		mb.add(menuAcercaDe);

		menuColorFondo = new JMenu("Color de fondo");
		menuColorFondo.setFont(new Font("Andale Mono", 1, 14));
		menuOpciones.add(menuColorFondo);

		boton1 = new JButton("Calcular");
		boton1.setBounds(30,350,100,30);
		boton1.setFont(new Font("Andale Mono",1, 14));
		boton1.addActionListener(this);
		add(boton1);

		miRojo = new JMenuItem("Rojo");
		miRojo.setFont(new Font("Andale Mono", 1, 14));
		miRojo.addActionListener(this);
		menuColorFondo.add(miRojo);

		miNegro = new JMenuItem("Negro");
		miNegro.setFont(new Font("Andale Mono", 1, 14));
		miNegro.addActionListener(this);
		menuColorFondo.add(miNegro);

		miMorado = new JMenuItem("Morado");
		miMorado.setFont(new Font("Andale Mono", 1, 14));
		miMorado.addActionListener(this);
		menuColorFondo.add(miMorado);

		miNuevo = new JMenuItem("Nuevo");
		miNuevo.setFont(new Font("Andale Mono", 1, 14));
		miNuevo.addActionListener(this);
		menuArchivo.add(miNuevo);

		miElCreador = new JMenuItem("El Creador");
		miElCreador.setFont(new Font("Andale Mono", 1, 14));
		miElCreador.addActionListener(this);
		menuAcercaDe.add(miElCreador);

		miSalir = new JMenuItem("Salir");
		miSalir.setFont(new Font("Andale Mono", 1, 14));
		miSalir.addActionListener(this);
		menuArchivo.add(miSalir);

		ImageIcon imagen = new ImageIcon("images/logo-coca.png");
		labelLogo = new JLabel(imagen);
		labelLogo.setBounds(5,5,250,100);
		add(labelLogo);

		labelBienvenido = new JLabel("Bienvenido " + nombreAdministrador);
		labelBienvenido.setBounds(280,30,300,50);
		labelBienvenido.setFont(new Font("Andale Mono", 1,32));
		labelBienvenido.setForeground(new Color(255,255,255));
		add(labelBienvenido);

		labelTitle = new JLabel("Datos del trabajador para el C�lculo");
		labelTitle.setBounds(45,140,900,25);
		labelTitle.setFont(new Font("Andale Mono", 0, 24));
		labelTitle.setForeground(new Color(255,255,255));
		add(labelTitle);

		labelNombre = new JLabel("Nombre completo");
		labelNombre.setBounds(25,188,180,25);
		labelNombre.setFont(new Font("Andale Mono", 1, 12));
		labelNombre.setForeground(new Color(255,255,255));
		add(labelNombre);

		txtNombreTrabajador = new JTextField();
		txtNombreTrabajador.setBounds(25,213,150,25);
		txtNombreTrabajador.setFont(new Font("Andale Mono", 1, 12));
		txtNombreTrabajador.setForeground(new Color(255,255,255));
		txtNombreTrabajador.setBackground(new java.awt.Color(224,224,224));
		add(txtNombreTrabajador);

		labelAPaterno = new JLabel("Apellidos");
		labelAPaterno.setBounds(25,250,180,25);
		labelAPaterno.setFont(new Font("Andale Mono", 1, 12));
		labelAPaterno.setForeground(new Color(255,255,255));
		add(labelAPaterno);

		txtAPaterno = new JTextField();
		txtAPaterno.setBounds(25,273,150,25);
		txtAPaterno.setFont(new Font("Andale Mono", 1, 12));
		txtAPaterno.setForeground(new Color(255,255,255));
		txtAPaterno.setBackground(new java.awt.Color(224,224,224));
		add(txtAPaterno);

		labelDepartamento = new JLabel("Selecciona Departamento");
		labelDepartamento.setBounds(220,188,180,25);
		labelDepartamento.setFont(new Font("Andale Mono", 1, 12));
		labelDepartamento.setForeground(new Color(255,255,255));
		add(labelDepartamento);

		comboDepartamento = new JComboBox();
		comboDepartamento.setBounds(220,213,220,25);
		comboDepartamento.setBackground(new java.awt.Color(224,224,224));
		comboDepartamento.setFont(new java.awt.Font("Andale Mono", 1,14));
		comboDepartamento.setForeground(new java.awt.Color(255,0,0));
		add(comboDepartamento);
		comboDepartamento.addItem("");
		comboDepartamento.addItem("Atenci�n al cliente");
		comboDepartamento.addItem("Departamento de Log�stica");
		comboDepartamento.addItem("Departamento de Gerencia");

		labelAntiguedad = new JLabel("Selecciona Antig�edad");
		labelAntiguedad.setBounds(220,248,180,25);
		labelAntiguedad.setFont(new Font("Andale Mono", 1, 12));
		labelAntiguedad.setForeground(new Color(255,255,255));
		add(labelAntiguedad);

		comboAntiguedad = new JComboBox();
		comboAntiguedad.setBounds(220,273,220,25);
		comboAntiguedad.setBackground(new java.awt.Color(224,224,224));
		comboAntiguedad.setFont(new java.awt.Font("Andale Mono", 1,14));
		comboAntiguedad.setForeground(new java.awt.Color(255,0,0));
		add(comboAntiguedad);
		comboAntiguedad.addItem("");
		comboAntiguedad.addItem("1 a�o de servicio");
		comboAntiguedad.addItem("2 a 6 a�os de servicio");
		comboAntiguedad.addItem("7 o m�s a�os de servicio");

		labelResultado = new JLabel("Resultado del C�lculo");
		labelResultado.setBounds(220,307,180,25);
		labelResultado.setFont(new Font("Andale Mono", 1, 12));
		labelResultado.setForeground(new Color(255,255,255));
		add(labelResultado);

		textarea1 = new JTextArea();
		textarea1.setEditable(false);
		textarea1.setBackground(new Color(224,224,224));
		textarea1.setFont(new Font("Andale Mono", 1, 11));
		textarea1.setForeground(new Color(255,255,255));
		textarea1.setText("\n Aqu� aparece el resultado del c�lculo.");
		scrollpane1 = new JScrollPane(textarea1);
		scrollpane1.setBounds(220,333,385,90);
		add(scrollpane1);

		labelfooter = new JLabel("�2019 The Coca-Cola Company | Todos los derechos reservados");
		labelfooter.setBounds(135,445,500,30);
		labelfooter.setFont(new Font("Andale Mono", 1, 12));
		labelfooter.setForeground(new Color(255,255,255));
		add(labelfooter);
	}

	public void actionPerformed(ActionEvent e){

		Container fondo = this.getContentPane();

		if (e.getSource() == boton1) {

			String nombreTrabajador = txtNombreTrabajador.getText();
			String AP = txtAPaterno.getText();
			String Departamento = comboDepartamento.getSelectedItem().toString();
			String Antiguedad = comboAntiguedad.getSelectedItem().toString();

			if (nombreTrabajador.equals("") || AP.equals("") || Departamento.equals("") || Antiguedad.equals("")) {
				
				JOptionPane.showMessageDialog(null, "Debes llenar todos los campos");

				} else {
					if (Departamento.equals("Atenci�n al cliente")) {
						if (Antiguedad.equals("1 a�o de servicio")) {
							textarea1.setText("\n El trabajador " + nombreTrabajador + " " + AP + " " + 
								              "\n quien labora en " + Departamento + " con " + Antiguedad +
								              "\n recibe 6 d�as de vacaciones");
							
						}
						if (Antiguedad.equals("2 a 6 a�os de servicio")) {
							textarea1.setText("\n El trabajador " + nombreTrabajador + " " + AP + " " + 
								              "\n quien labora en " + Departamento + " con " + Antiguedad +
								              "\n recibe 14 d�as de vacaciones");
						}
						if (Antiguedad.equals("7 o m�s a�os de servicio")) {
							textarea1.setText("\n El trabajador " + nombreTrabajador + " " + AP + " " + 
								              "\n quien labora en " + Departamento + " con " + Antiguedad +
								              "\n recibe 20 d�as de vacaciones");
						}
						
					}
					if (Departamento.equals("Departamento de Log�stica")) {
						if (Antiguedad.equals("1 a�o de servicio")) {
							textarea1.setText("\n El trabajador " + nombreTrabajador + " " + AP + " " + 
								              "\n quien labora en " + Departamento + " con " + Antiguedad +
								              "\n recibe 7 d�as de vacaciones");
							
						}
						if (Antiguedad.equals("2 a 6 a�os de servicio")) {
							textarea1.setText("\n El trabajador " + nombreTrabajador + " " + AP + " " + 
								              "\n quien labora en " + Departamento + " con " + Antiguedad +
								              "\n recibe 15 d�as de vacaciones");
						}
						if (Antiguedad.equals("7 o m�s a�os de servicio")) {
							textarea1.setText("\n El trabajador " + nombreTrabajador + " " + AP + " " + 
								              "\n quien labora en " + Departamento + " con " + Antiguedad +
								              "\n recibe 22 d�as de vacaciones");
						}
					}

					if (Departamento.equals("Departamento de Gerencia")) {
						if (Antiguedad.equals("1 a�o de servicio")) {
							textarea1.setText("\n El trabajador " + nombreTrabajador + " " + AP + " " + 
								              "\n quien labora en " + Departamento + " con " + Antiguedad +
								              "\n recibe 10 d�as de vacaciones");
							
						}
						if (Antiguedad.equals("2 a 6 a�os de servicio")) {
							textarea1.setText("\n El trabajador " + nombreTrabajador + " " + AP + " " + 
								              "\n quien labora en " + Departamento + " con " + Antiguedad +
								              "\n recibe 20 d�as de vacaciones");
						}
						if (Antiguedad.equals("7 o m�s a�os de servicio")) {
							textarea1.setText("\n El trabajador " + nombreTrabajador + " " + AP + " " + 
								              "\n quien labora en " + Departamento + " con " + Antiguedad +
								              "\n recibe 30 d�as de vacaciones");
						}
					}

			}


		} 

		if (e.getSource() == miRojo) {

			fondo.setBackground(new Color(255,0,0));
			mb.setBackground(new Color(255,0,0));


		} 

		if (e.getSource() == miNegro) {

			fondo.setBackground(new Color(0,0,0));
			mb.setBackground(new Color(0,0,0));	

		} 

		if (e.getSource() == miMorado) {

			fondo.setBackground(new Color(153,0,255));
			mb.setBackground(new Color(153,0,255));

		} 

		if (e.getSource() == miNuevo) {

			Bienvenida ventanabienvenida = new Bienvenida();
			ventanabienvenida.setBounds(0,0,350,450);
			ventanabienvenida.setResizable(false);
			ventanabienvenida.setVisible(true);
			ventanabienvenida.setLocationRelativeTo(null);
			this.setVisible(false);

		} 

		if (e.getSource() == miSalir) {

			Bienvenida ventanabienvenida = new Bienvenida();
			ventanabienvenida.setBounds(0,0,350,450);
			ventanabienvenida.setResizable(false);
			ventanabienvenida.setVisible(true);
			ventanabienvenida.setLocationRelativeTo(null);
			this.setVisible(false);

		} 

		if (e.getSource() == miElCreador) {

			JOptionPane.showMessageDialog(null, "Primer programa crado por Antony Giomar");

		} 
	}

	public static void main(String args[]){

		Principal ventanaPrincipal = new Principal();
		ventanaPrincipal.setBounds(0,0,640,535);
		ventanaPrincipal.setVisible(true);
		ventanaPrincipal.setResizable(false);
		ventanaPrincipal.setLocationRelativeTo(null);
	}
}